from django.contrib import admin
from barcamp.core.models import Chapter, Edition, Session

admin.site.register(Chapter)
admin.site.register(Edition)
admin.site.register(Session)

