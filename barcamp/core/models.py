from django.db import models

class Chapter(models.Model):
    """Model to store chapter information. Such as "Barcamp Mumbai" """
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    url = models.URLField()
    
    def __unicode__(self):
        return self.name


class Edition(models.Model):
    """Edition/ number such as "Barcamp Mumbai 8" """
    chapter = models.ForeignKey('Chapter')
    number = models.IntegerField()
    
    venue = models.TextField()
    date = models.DateField()
    time_from = models.TimeField()
    time_to = models.TimeField()
    
    contact_person = models.CharField(max_length=100)
    contact_email = models.EmailField()
    contact_phone = models.CharField(max_length=25)

    def __unicode__(self):
        return "%s %s" %(self.chapter.name, self.number)

    
class Session(models.Model):
    """Session at a Barcamp edition."""
    edition = models.ForeignKey('Edition')
    
    speaker = models.CharField(max_length=100)
    speaker_url = models.URLField()
    
    topic = models.TextField()
    venue = models.CharField(max_length=100)
    time = models.TimeField()
    
    def __unicode__(self):
        return "[%s %s] %s : %s" %(
            self.edition.chapter.name,
            self.edition.number,
            self.speaker,
            self.topic,
        )