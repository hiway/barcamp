from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext

from barcamp.core.models import Chapter, Edition, Session

def index(request):
    """View to generate home page that contains an overview of current and
    upcoming barcamps and sessions."""
    
    chapters = Chapter.objects.all()
    
    return render_to_response("index.html", {
            "chapters":chapters,
            },
            context_instance=RequestContext(request))


def chapter(request, chapter_slug):
    """View to display details of a chapter: all past, current and upcoming
    editions."""
    chapter = get_object_or_404(Chapter.objects.get(slug=chapter_slug))
    editions = Edition.objects.filter(chapter=chapter)
    return render_to_response("chapter.html", {
            "chapter":chapter,
            "editions":editions,
            },
            context_instance=RequestContext(request))

def edition(request, chapter_slug, edition_number):
    """View to display all sessions hosted at a barcamp edition."""
    chapter = get_object_or_404(Chapter.objects.get(slug=chapter_slug))
    edition = get_object_or_404(Edition.objects.get(number=edition_number))
    sessions = Session.objects.filter(edition=edition)
    return render_to_response("edition.html", {
            "chapter":chapter,
            "edition":edition,
            "sessions":sessions,
            },
            context_instance=RequestContext(request))
