from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^(?P<chapter_slug>((\w+)|-)+)/(?P<edition_number>(\d+))/$',
        'barcamp.web.views.edition', name='edition'),
    url(r'^(?P<chapter_slug>((\w+)|-)+)/$', 'barcamp.web.views.chapter', name='chapter'),
    url(r'^$', 'barcamp.web.views.index', name='index'),
)
